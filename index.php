<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
    $created_at = $request->getParsedBodyParam('created_at','');
  $updated_at = $request->getParsedBodyParam('updated_at','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->created_at=$created_at;
 $_message->updated_at=$updated_at;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']);
    $_message->delete();
    if($_message->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    //die("message id is " . $_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

//messages bulk---------------------------------------------------------
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});
//2) הוסיפו route שמאפשר להוסיף קבוצה של users
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();// שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});
//3) הוסיפו route שמאפשר למחוק קבוצה של users
$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();// שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 

    User::trash($payload);
    return $response->withStatus(201)->withJson($payload);
});


//user hw---------------------------------------------------------


$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});
//1) השלימו את post של User
$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
     $created_at = $request->getParsedBodyParam('created_at','');
  $updated_at = $request->getParsedBodyParam('updated_at','');
   $_user = new User();
   $_user->username=$username;
     $_user->created_at=$created_at;
 $_user->updated_at=$updated_at;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->run();